#-*- coding: utf-8-*-

""" test_guess.py is test code for my guess.py program (game).""" 
import guess
import unittest
import pexpect 
class TestFactorialFunctions(unittest.TestCase): 

	def setUp(self):
	    self.child = None 
	    self.hello_message = "Hello there, what is your name?"
	    self.greet1_message = ( "Lets play a game. I am thinking of a number between 1 and 10.") 
	    self.greet2_message = ( "Take a guess. Good Luck!") 
	    self.number_message = "Whats my number? Enter a number [1-10]: " 
	    self.too_low_message = "Im sorry :(, your guess is too low. "
	    self.too_high_message = "Try again, your guess is too high. "
	    self.success_message = "Good job! You guessed my number! " 
	    self.error_message = "Im sorry, The number I was thinking of was actually"
# added 4 more strings. Pass

def test_greet(self): 
	pass 
		# number on its own (no --number option) 

def test_get_user_guess(self): 
    self.child = pexpect.spawn("python guess.py --number=7")     
    self.child.expect_exact(self.greet1_message, timeout=1)     
    self.child.expect_exact(self.greet2_message, timeout=1)    
    self.child.expect_exact(self.number_message, timeout=1) 
    self.child.sendline("0") 
    self.child.expect_exact(self.error_message, timeout=1) 
    self.child.expect_exact(self.number_message, timeout=1)  
    self.child.sendline("99") 
    self.child.expect_exact(self.error_message, timeout=1)     
    self.child.expect_exact(self.number_message, timeout=1)    
    self.child.sendline("foo") 
    self.child.expect_exact(self.error_message, timeout=1) 
    self.child.expect_exact(self.number_message, timeout=1) 

def test_guess_correctly(self): 
	pass  


def test_hint(self): 
	self.child = pexpect.spawn("python guess.py 	--number=7") 
	self.child.expect_exact(self.greet1_message, timeout=1) 
	self.child.expect_exact(self.greet2_message, timeout=1) 
	self.child.expect_exact(self.number_message, timeout=1) 
	self.child.sendline("1") 

def too_low_message(self):
	self.child = pexpect.spawn("python guess.py 	--number=7") 
	self.child.expect_exact(self.too_low_message, timeout=1) 
	self.child.expect_exact(self.number_message, timeout=1) 
	self.child.sendline("10") 

def too_high_message(self):
	self.child = pexpect.spawn("python guess.py 	--number=7") 
	self.child.expect_exact(self.too_high_message, timeout=1) 
	self.child.expect_exact(self.number_message, timeout=1) 
	self.child.sendline("7") 
	self.child.expect_exact(self.success_message, timeout=1)


def test_error_message(self): 
	self.child = pexpect.spawn("python guess.py --number=11") 
	self.child.expect_exact(self.greet1_message, timeout=1) 
	self.child.expect_exact(self.greet2_message, timeout=1) 
	self.child.expect_exact(self.number_message, timeout=1) 
	self.child.sendline("0")
	self.child.expect_exact(self.error_message, timeout=1) 
	self.child.expect_exact(self.number_message, timeout=1) 
	self.child.sendline("11") 
	self.child.expect_exact(self.error_message, timeout=1) 
	self.child.expect_exact(self.number_message, timeout=1) 
	self.child.sendline("foo") 
	self.child.expect_exact(self.error_message, timeout=1) 
	self.child.expect_exact(self.number_message, timeout=1) 

# added tests