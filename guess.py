import random

guessesTaken = 0
print('Hello there! What is your name?')
myName = input()
number = random.randint(1, 10)

print('Well, ' + myName + ', Lets play a game. I am thinking of a number between 1 and 10.')

while guessesTaken < 4:
     print('Take a guess. Good Luck!') 
# There are five spaces in front of print.

    guess = input()
    guess = int(guess)
    guesses = guessesTaken + 1
    if guess < number:
        print('Im sorry :(, your guess is too low.')
 # There are eight spaces in front of print.

    if guess > number:
        print('Try again, your guess is too high.')
    if guess == number:

        break

if guess == number:
    guessesTaken = str(guessesTaken)
    print('Good job, ' + myName + '! You guessed my number in ' + guessesTaken + ' guesses!')

if guess != number:
    number = str(number)
    print('Im sorry, '+ myName + ' The number I was thinking of was actually ' + number)


if __name__=='__main__':
	main()

